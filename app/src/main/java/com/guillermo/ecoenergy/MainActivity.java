package com.guillermo.ecoenergy;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText name = findViewById(R.id.editTextText);
        EditText address = findViewById(R.id.editTextAdress);
        EditText phone = findViewById(R.id.editTextPhone);
        EditText email = findViewById(R.id.editTextTextEmailAddress);

        Button buttonSubmit = findViewById(R.id.enviarButton);

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = "Name: " + name.getText().toString() + "n" +
                        "Address: " + address.getText().toString() + "n" +
                        "Phone: " + phone.getText().toString() + "n" +
                        "Email: " + email.getText().toString() + "n";
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
             }
        });
    }
}
